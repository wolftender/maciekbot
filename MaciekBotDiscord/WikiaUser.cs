﻿using System;
using System.Xml;
using System.Net;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

namespace MaciekBotDiscord
{
	public class WikiaUser
	{
		private string userID;
		private string userName;
		private string userEdits;
		private string userJoin;
		private string userAvatar;
		private List<string> globalRights;

		public string UserID { get { return userID; } }
		public string UserName { get { return userName; } }
		public string UserEdits { get { return userEdits; } }
		public string UserJoin { get { return userJoin; } }
		public string UserAvatar { get { return userAvatar; } }
		public List<string> GlobalRights { get { return globalRights; } }

		private WikiaUser ( )
		{
			globalRights = new List<string> ( );
		}

		public static WikiaUser Get ( string username )
		{
			string request = "http://pl.c.wikia.com/api.php?action=query&list=users&ususers=" + username + "&usprop=groups|editcount|registration&format=xml";
			WikiaUser user = new WikiaUser ( );

			XmlDocument xml = new XmlDocument ( );
			xml.Load ( request );

			XmlNode userNode = xml.DocumentElement.FirstChild.FirstChild.FirstChild;

			if ( userNode.Attributes ["missing"] == null )
			{
				Console.WriteLine ( "[Info] User found!" );

				user.userID = userNode.Attributes ["userid"].Value;
				user.userName = userNode.Attributes ["name"].Value;
				user.userEdits = userNode.Attributes ["editcount"].Value;
				user.userJoin = userNode.Attributes ["registration"].Value;

				foreach ( XmlNode node in userNode.FirstChild.ChildNodes )
				{
					string right = node.InnerText;

					if ( right == "poweruser" || right == "helper" || right == "staff" || right == "util" || right == "vstf" )
					{
						user.globalRights.Add ( right );
					}
				}

				using ( WebClient wc = new WebClient ( ) )
				{
					var json = wc.DownloadString ( "http://pl.c.wikia.com/api/v1/User/Details?ids=" + user.userID );

					JObject obj = JObject.Parse ( json );
					user.userAvatar = obj ["items"] [0] ["avatar"].Value<string> ( );
				}

				if ( user.userJoin == null || user.userJoin.Length == 0 )
					user.userJoin = "N/A";

				return user;
			}

			return null;
		}
	}
}
