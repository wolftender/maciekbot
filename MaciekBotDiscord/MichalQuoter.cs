﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace MaciekBotDiscord
{
	public class MichalQuoter
	{
		private List<string> quotes;

		public List <string> Quotes
		{
			get { return quotes; }
		}

		public MichalQuoter ( )
		{
			quotes = new List<string> ( );
			Reload ( );
		}

		public void Reload ( )
		{
			quotes.Clear ( );

			XmlDocument xml = new XmlDocument ( );
			xml.Load ( "http://pakokany.hostoi.com/michalbr/api.php" );

			XmlNodeList list = xml.GetElementsByTagName ( "quote" );

			foreach ( XmlNode node in list )
			{
				quotes.Add ( node.InnerText );
			}
		}

		public string Randomize ( )
		{
			Random rnd = new Random ( );
			int random = rnd.Next ( 0, quotes.Count - 1 );

			return quotes [random];
		}
	}
}
