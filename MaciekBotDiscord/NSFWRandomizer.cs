﻿using System;
using System.Xml;

namespace MaciekBotDiscord
{
	public class NSFWRandomizer
	{
		public NSFWRandomizer ( ) { }

		private string MakeURL ( string tags, int pageID )
		{
			string uri = "http://rule34.xxx/index.php?page=dapi&s=post&q=index&limit=1&tags=" + tags + "&pid=" + pageID.ToString ( );
			return Uri.EscapeUriString ( uri );
		}

		public string Randomize ( string tags )
		{
			XmlDocument xml = new XmlDocument ( );
			xml.Load ( MakeURL ( tags, 0 ) );

			int postCount = int.Parse ( xml.DocumentElement.GetAttribute ( "count" ) );

			if ( postCount == 0 )
				return null;

			Random rnd = new Random ( );
			int random = rnd.Next ( 0, postCount - 1 );

			xml.Load ( MakeURL ( tags, random ) );

			return "http:" + xml.DocumentElement.FirstChild.Attributes ["file_url"].Value;
		}
	}
}
