﻿using System;
using System.IO;
using System.Collections.Generic;

using Google.Apis.Services;
using Google.Apis.YouTube.v3;

namespace MaciekBotDiscord
{
	public class SongRandomizer
	{
		private string apiKey = "";
		private string playlistID = "";

		public SongRandomizer ( string playlist = "PL5Hmwn2yVyJITghy-wQDMzoZmpABgcYY4" )
		{
			apiKey = File.ReadAllText ( "youtube.txt" );
			playlistID = playlist;
		}

		public string RandomizeSong ( )
		{
			var youtubeService = new YouTubeService ( new BaseClientService.Initializer ( )
			{
				ApiKey = apiKey,
				ApplicationName = GetType ( ).ToString ( )
			} );

			string nextPageToken = "";

			List <string> videos = new List<string> ( );

			while ( nextPageToken != null )
			{
				var playlistItemsRequest = youtubeService.PlaylistItems.List ( "snippet" );
				playlistItemsRequest.PlaylistId = playlistID;
				playlistItemsRequest.MaxResults = 50;
				playlistItemsRequest.PageToken = nextPageToken;

				var playlistItemsResponse = playlistItemsRequest.Execute ( );

				nextPageToken = playlistItemsResponse.NextPageToken;
				
				foreach ( var playlistItem in playlistItemsResponse.Items )
				{
					videos.Add ( playlistItem.Snippet.ResourceId.VideoId );
				}

				playlistItemsRequest = null;
				playlistItemsResponse = null;
			}

			youtubeService.Dispose ( );

			Random rnd = new Random ( );
			int random = rnd.Next ( 0, videos.Count - 1 );

			string randomVideo = videos [random];
			videos = null;

			return randomVideo;
		}
	}
}
