﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

namespace MaciekBotDiscord
{
	class Program
	{
		private DiscordSocketClient socketClient;
		private CommandService commands;
		private IServiceProvider services;
		private MichalQuoter quoter;
		private MichalSpammer spammer;

		public static void Main ( string [] args )
			=> new Program ( ).MainAsync ( ).GetAwaiter ( ).GetResult ( );

		public async Task MainAsync ( )
		{
			// Loading XML config
			string token = File.ReadAllText ( "api.txt" );

			socketClient = new DiscordSocketClient ( );
			commands = new CommandService ( );

			quoter = new MichalQuoter ( );
			spammer = new MichalSpammer ( quoter );

			services = new ServiceCollection ( )
				.AddSingleton ( new SongRandomizer ( ) )
				.AddSingleton ( new NSFWRandomizer ( ) )
				.AddSingleton ( spammer )
				.AddSingleton ( quoter )
				.AddSingleton ( new WikiaService ( ) )
				.AddSingleton ( socketClient )
				.BuildServiceProvider ( );

			// Handlers
			socketClient.Log += Log;

			await socketClient.LoginAsync ( TokenType.Bot, token );
			await socketClient.StartAsync ( );

			await InitializeCommands ( );

			while ( true )
			{
				string command = Console.ReadLine ( );

				if ( command == "stop" )
					break;
			}
		}

		public async Task InitializeCommands ( )
		{
			socketClient.MessageReceived += HandleCommand;

			await commands.AddModulesAsync ( Assembly.GetEntryAssembly ( ) );
		}

		private Task Log ( LogMessage msg )
		{
			Console.WriteLine ( msg.ToString ( ) );
			return Task.CompletedTask;
		}

		private async Task HandleCommand ( SocketMessage messageParam )
		{
			var message = messageParam as SocketUserMessage;
			if ( message == null ) return;

			if ( message.Author.Id != socketClient.CurrentUser.Id ) await spammer.OnMessage ( messageParam );

			int argPos = 2;

			// if ( !message.HasCharPrefix ( '/', ref argPos ) ) return;
			if ( message.Content.Substring ( 0, 2 ) != "m!" ) return;

			var context = new CommandContext ( socketClient, message );

			var result = await commands.ExecuteAsync ( context, argPos, services );
			if ( !result.IsSuccess )
				await context.Channel.SendMessageAsync ( result.ErrorReason );
		}
	}
}
