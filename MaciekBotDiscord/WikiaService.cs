﻿using System;
using System.Xml;
using System.Collections.Generic;

using Discord;

namespace MaciekBotDiscord
{
	public class WikiaService
	{
		private List<string> coolUsers;
		private List<string> uncoolUsers;

		public WikiaService ( )
		{
			coolUsers = new List<string> ( );
			uncoolUsers = new List<string> ( );

			UpdateRatingOverrides ( );
		}

		public void UpdateRatingOverrides ( )
		{
			coolUsers.Clear ( );
			uncoolUsers.Clear ( );

			XmlDocument xml = new XmlDocument ( );
			xml.Load ( "overrides.xml" );

			XmlNodeList list = xml.GetElementsByTagName ( "override" );

			foreach ( XmlNode node in list )
			{
				if ( node.Attributes ["type"].Value == "cool" )
					coolUsers.Add ( node.Attributes ["username"].Value );
				else
					uncoolUsers.Add ( node.Attributes ["username"].Value );
			}
		}

		/*
		 * Wikia User Rating
		 * max. 50 points for edits, 1 for each 200
		 */

		private int Clamp ( int n, int min, int max )
		{
			if ( n > max ) n = max;
			if ( n < min ) n = min;

			return n;
		}

		public string RateUser ( string username )
		{
			WikiaUser user = WikiaUser.Get ( username );

			if ( user != null )
			{
				int rating = 0;
				rating += Clamp ( int.Parse ( user.UserEdits ) / 200, 0, 75 );
				
				foreach ( string right in user.GlobalRights )
				{
					if ( right == "staff" ) rating += 10;
					if ( right == "util" ) rating += 10;
					if ( right == "helper" ) rating += 5;
					if ( right == "vstf" ) rating += 5;
					if ( right == "poweruser" ) rating += 20;
				}

				rating = Clamp ( rating, 0, 100 );

				if ( coolUsers.Contains ( user.UserName ) ) { rating = 100; }
				if ( uncoolUsers.Contains ( user.UserName ) ) { rating = -1; }

				return ":thinking: | Hmm... Wiem! Oceniam użytkownika **" + username + "** na **" + rating + "/100** punktów!";
			}
			else
			{
				return "Użytkownik o nazwie " + username + " nie został odnaleziony!";
			}
		}

		public Embed MakeUserInfo ( string username )
		{
			EmbedBuilder builder = new EmbedBuilder ( );
			Console.WriteLine ( "[Info] Searching for " + username + "..." );

			builder.WithColor ( new Color ( 66, 161, 244 ) );

			WikiaUser user = WikiaUser.Get ( username );

			if ( user != null )
			{
				builder.WithTitle ( "Użytkownik: " + user.UserName );
				builder.WithThumbnailUrl ( user.UserAvatar );
				builder.WithDescription ( "Poniżej przedstawione są informacje o użytkowniku na portalu FANDOM." );

				builder.AddInlineField ( "Edycje Globalne", user.UserEdits );
				builder.AddInlineField ( "Dołączył", user.UserJoin );

				string globalRightsText = "";

				if ( user.GlobalRights.Count > 0 )
				{
					foreach ( string right in user.GlobalRights )
						globalRightsText += right + " ";
				}
				else
				{
					globalRightsText = "Użytkownik nie posiada uprawnień globalnych.";
				}

				builder.AddInlineField ( "Ważne Linki", "[Profil](pl.c.wikia.com/wiki/User:" + user.UserName + ") | [Tablica Wiadomości](pl.c.wikia.com/wiki/Message_Wall:" + user.UserName + ")" );
				builder.AddInlineField ( "Uprawnienia Globalne", globalRightsText );
			}
			else
			{
				Console.WriteLine ( "[Info] User not found!" );
				builder.WithTitle ( "Nie znaleziono" );
				builder.WithDescription ( "Użytkownik o nazwie " + username + " nie został znaleziony!" );
			}

			return builder.Build ( );
		}
	}
}
