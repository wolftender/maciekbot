﻿using System;
using System.Threading.Tasks;

using Discord.WebSocket;
using Discord.Commands;
using Discord;

namespace MaciekBotDiscord
{
	public class WikiaModule : ModuleBase
	{
		private WikiaService service;

		public WikiaModule ( WikiaService service )
		{
			this.service = service;
		}

		[Command ( "rateuser" ), Summary ( "Rates a Wikia user" )]
		public async Task RateWikiUser ( )
		{
			await ReplyAsync ( "Zgubiłeś parametr! Poprawne użycie: ``m!rateuser <nazwa>``" );
		}

		[Command ( "rateuser" ), Summary ( "Rates a Wikia user" )]
		public async Task RateWikiUser ( [Remainder, Summary ( "User to check" )] string username )
		{
			await ReplyAsync ( service.RateUser ( username ) );
		}

		[Command ( "reload-rateuser" ), Summary ( "Reloads rateuser command overrider" ), RequireOwner]
		public async Task ReloadRateUser ( )
		{
			service.UpdateRatingOverrides ( );
			Console.WriteLine ( "[Info] Reloaded User Rating Overrides!" );
			await ReplyAsync ( "Załadowałam wszystkie nadpisania ocen użytkowników ponownie z pliku XML :smile:" );
		}

		[Command ( "wikiuser" ), Summary ( "Displays information about wiki user" )]
		public async Task WikiUser ( )
		{
			await ReplyAsync ( "Zgubiłeś parametr! Poprawne użycie: ``m!wikiuser <nazwa>``" );
		}

		[ Command ( "wikiuser" ), Summary ( "Displays information about wiki user" ) ]
		public async Task WikiUser ( [Remainder, Summary ( "User to check" )] string username )
		{
			await ReplyAsync ( "", false, service.MakeUserInfo ( username ) );
		}
	}
}
