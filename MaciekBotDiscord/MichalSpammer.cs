﻿using System.Threading.Tasks;

using Discord.WebSocket;

namespace MaciekBotDiscord
{
	public class MichalSpammer
	{
		private MichalQuoter quoter;

		public bool Enabled;

		public MichalSpammer ( MichalQuoter quoter )
		{
			this.quoter = quoter;
			this.Enabled = false;
		}

		public async Task OnMessage ( SocketMessage messageParam )
		{
			if ( Enabled )
			{
				await messageParam.Channel.SendMessageAsync ( quoter.Randomize ( ) );
			}
		}
	}
}
