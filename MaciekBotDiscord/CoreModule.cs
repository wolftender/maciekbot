﻿using System;
using System.IO;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

namespace MaciekBotDiscord
{
	public class CoreModule : ModuleBase
	{
		private SongRandomizer songRandomizer;
		private NSFWRandomizer nsfwRandomizer;
		private MichalQuoter michalQuotes;
		private DiscordSocketClient discordClient;
		private MichalSpammer spammer;

		public CoreModule ( 
			DiscordSocketClient client, 
			SongRandomizer songRandomizer, 
			NSFWRandomizer nsfwRandomizer, 
			MichalQuoter quoter,
			MichalSpammer spammer )
		{
			this.songRandomizer = songRandomizer;
			this.nsfwRandomizer = nsfwRandomizer;
			this.spammer = spammer;
			this.michalQuotes = quoter;
			this.discordClient = client;
		}

		[Command ( "tts" ), Summary ( "Broadcasts a TTS message" ), RequireOwner]
		public async Task TTSSay ( [Remainder, Summary ( "Text to say" )] string text )
		{
			await Context.Message.DeleteAsync ( );
			await ReplyAsync ( text, true );
		}

		[Command ( "spammer" ), Summary ( "Broadcasts a message" ), RequireOwner]
		public async Task ToggleSpammer ( )
		{
			spammer.Enabled = !spammer.Enabled;
			
			if ( spammer.Enabled )
				await ReplyAsync ( ":warning: | **Uwaga!** Moduł spamu aktywowany!" );
			else
				await ReplyAsync ( "Moduł spamowania wyłączony!" );
		}

		[ Command ( "delete" ), Summary ( "Removes messages from channel" ) ]
		[ RequireUserPermission ( GuildPermission.Administrator ) ]
		[ RequireBotPermission ( GuildPermission.ManageMessages ) ]
		public async Task Purge ( uint ammount )
		{
			var messages = await Context.Channel.GetMessagesAsync ( ( int ) ammount + 1 ).Flatten ( );
			await Context.Channel.DeleteMessagesAsync ( messages );

			var infoMessage = await ReplyAsync ( ":warning: | Według rozkazu **" + Context.User.Username + "** usunęłam **" + ammount + "** wiadomości z tego kanału!" );
			Console.WriteLine ( "[Info] User " + Context.User.Username + " just bulk deleted " + ammount + " messages!" );

			await Task.Delay ( 3500 );
			await infoMessage.DeleteAsync ( );
		}


		[ Command ( "say" ), Summary ( "Broadcasts a message" ), RequireOwner ]
		public async Task Say ( [ Remainder, Summary ( "Text to say" ) ] string text )
		{
			await Context.Message.DeleteAsync ( );
			Console.WriteLine ( "[Say] " + text );
			await ReplyAsync ( text );
		}

		[ Command ( "help" ), Summary ( "Displays Help" ) ]
		public async Task Help ( )
		{
			string help = File.ReadAllText ( "help.txt" );

			EmbedBuilder builder = new EmbedBuilder ( );

			builder.WithTitle ( "Lista komend MaciekBota" );
			builder.WithDescription ( help );

			await ReplyAsync ( "", false, builder.Build ( ) );
		}

		[Command ( "setstream" ), Summary ( "Sets bot to stream something" ), RequireOwner]
		public async Task Stream ( [Remainder, Summary ( "What to stream" )] string name )
		{
			Console.WriteLine ( "[Info] Pretending to stream " + name );
			await discordClient.SetGameAsync ( name, null, StreamType.Twitch );
		}

		[ Command ( "setgame" ), Summary ( "Sets bot to play something" ), RequireOwner ]
		public async Task PlayGame ( [Remainder, Summary ( "What to stream" )] string name )
		{
			Console.WriteLine ( "[Info] Pretending to play " + name );
			await discordClient.SetGameAsync ( name );
		}

		[ Command ( "piosenka" ), Summary ( "Links a random music video on YouTube" ) ]
		public async Task RandomSong ( )
		{
			string randomSong = songRandomizer.RandomizeSong ( );

			Console.WriteLine ( "[Info] Randomized Song: " + randomSong );
			await ReplyAsync ( "https://www.youtube.com/watch?v=" + randomSong );
		}

		[Command ( "reload-michal" ), Summary ( "Reloads quote XML" ), RequireOwner]
		public async Task ReloadQuotes ( )
		{
			michalQuotes.Reload ( );

			Console.WriteLine ( "[Info] Reloaded quotes!" );
			await ReplyAsync ( "Załadowałam wszystkie cytaty ponownie z pliku XML. Bawcie się dobrze :wink:" );
		}

		[Command ( "michal-dump" ), Summary ( "Dumps all quotes" ), RequireOwner]
		public async Task DumpQuotes ( )
		{
			foreach ( string quote in michalQuotes.Quotes )
			{
				await ReplyAsync ( quote );
			}
		}

		[ Command ( "michal" ), Summary ( "Displays random Michalbr10 quote" ) ]
		public async Task RandomQuote ( )
		{
			string randomQuote = michalQuotes.Randomize ( );

			Console.WriteLine ( "[Info] Randomized Quote: " + randomQuote );
			await ReplyAsync ( randomQuote );
		}

		[ Command ( "michal-tts" ), Summary ( "TTS Sends random Michalbr10 quote" ) ]
		public async Task RandomTTSQuote ( )
		{
			string randomQuote = michalQuotes.Randomize ( );

			Console.WriteLine ( "[Info] Randomized Quote: " + randomQuote );
			await ReplyAsync ( randomQuote, true );
		}

		[Command ( "rule34" ), Summary ( "Gets a post from Rule 34" )]
		[Alias ( "nsfw" )]
		public async Task NSFWPost (  )
		{
			await ReplyAsync ( "Zgubiłeś parametr! Poprawne użycie: ``m!nsfw <tag>``" );
		}

		[ Command ( "rule34" ), Summary ( "Gets a post from Rule 34" ) ]
		[ Alias ( "nsfw" ) ]
		public async Task NSFWPost ( [ Remainder, Summary ( "Tags" ) ] string tags )
		{
			if ( Context.Channel.IsNsfw )
			{
				Console.WriteLine ( "[Info] Obtaining random R34 post with tags: " + tags );
				string post = nsfwRandomizer.Randomize ( tags );

				if ( post == null )
				{
					Console.WriteLine ( "[Info] Nothing found!" );
					await ReplyAsync ( "Brak wyników dla tagów: **" + tags + "**!" );
				}
				else
				{
					Console.WriteLine ( "[Info] Post found: " + post );
					await ReplyAsync ( post );
					await ReplyAsync ( "Baw się dobrze :wink:" );
				}
			}
			else
			{
				await ReplyAsync ( "Ładnie to tak być zboczuchem? To nie kanał NSFW!" );
			}
		}
	}
}
